#!/bin/bash

set -e

PAGE_COUNT_FILE="pagecount.csv"
WORD_COUNT_FILE="wordcount.csv"
WANT_PAGE_COUNT="n" # Change to 'y' if page count should be calculated. Requires
                    # full complication and is thus much slower than word count.

get_word_count() {
    local out="$1"
    [ -f "$out" ] || return 1

    texcount -total -sum -1 -q *.tex 2>/dev/null | egrep -v "^( |$)" >> "$out"
}

get_page_count() {
    local out="$1"
    [ -f "$out" ] || return 1

    # Might want to change these
    local tex_file="kandi.tex"
    local pdf_file="kandi.pdf"

    latexmk -quiet -pdf -pdflatex=xelatex -latexoption="-shell-escape -halt-on-error" "$tex_file" \
        || { echo "couldn't compile"; return 0; }

    pdfinfo "$pdf_file" | awk '/^Pages: /{ print $2 }' >> "$out"
}

print_commit_stats() {
    local out_words="$1"

    [ "$out_words" ] || return 1

    local day=
    day=$(git show --no-patch --format="%ai" | awk '!/$^/{ print $1 }')

    if [ "$WANT_PAGE_COUNT" = y ]
    then
        local out_pages="$2"
        [ "$out_pages" ] || return 1

        touch "$out_pages"
        echo -n "$day " >> "$out_pages"
        get_page_count "$out_pages"

        if ! git diff-files --quiet
        then
            git stash && git stash drop
        fi
    fi

    touch "$out_words"
    echo -n "$day " >> "$out_words"
    get_word_count "$out_words"
}

get_stats_for_each_commit() {
    local repo="$1"

    [ -d "$repo" ] || return 1

    export -f print_commit_stats
    export -f get_page_count
    export -f get_word_count
    export WANT_PAGE_COUNT

    local out_words="$PWD/$WORD_COUNT_FILE"
    local out_pages="$PWD/$PAGE_COUNT_FILE"

    GIT_EDITOR=cat git -C "$repo" rebase -i --root master \
              --exec "print_commit_stats $out_words $out_pages"
}

if [ ! -d "$1" ]
then
    echo "Calculate word and page count for each commit in git history"
    echo "Usage: $0 <git_repo_to_measure>"
    echo "-------------------------------"
    echo -n "Warning: uses git-rebase to execute a command on each commit, which"
    echo " will modify commit date on each commit."
    echo -n "Check that there is nothing valuable unstaged or unpushed, and do"
    echo " 'git reset origin/master --hard' afterwards."
    echo
    exit 1
fi

get_stats_for_each_commit "$1"
